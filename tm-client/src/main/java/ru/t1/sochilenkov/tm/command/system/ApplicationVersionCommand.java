package ru.t1.sochilenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.sochilenkov.tm.dto.response.ApplicationVersionResponse;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Display version of the program.";

    @NotNull
    public static final String NAME = "version";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");

        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();

        ApplicationVersionResponse response = getSystemEndpoint().getVersion(request);
        System.out.println(response.getVersion());
    }

}
